package utfpr.ct.dainf.if62c.pratica;


/**
 * Representa uma matriz de valores {@code double}.
 * @author Wilson Horstmeyer Bogadao <wilson@utfpr.edu.br>
 */
public class Matriz {
    
    // a matriz representada por esta classe
    private final double[][] mat;
    public int linhasM;
    public int colunasM;
   
    public Matriz(){
        int m = this.linhasM;
        int n = this.colunasM;
        mat = new double[0][0];
    }
    
    /**
     * Construtor que aloca a matriz.
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz(int m, int n) throws MatrizInvalidaException {
        if(m<=0 || n<=0){
            linhasM = m;
            colunasM =n;
            
            throw new MatrizInvalidaException(m,n);
        }
        mat = new double[m][n];
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz()  {
        return mat;
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz getTransposta() throws MatrizInvalidaException {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     */
    public Matriz soma(Matriz m) throws SomaMatrizesIncompativeisException, MatrizInvalidaException {
        //throw new UnsupportedOperationException("Soma de matrizes não implementada.");
        //double somaMatriz = 0;
        //SomaMatrizesIncompativeisException M = new SomaMatrizesIncompativeisException();
        //Matriz m1 = m.getMatriz();
        //Matriz m2 = this.getMatriz();
        //double[][] m2 = this.getMatriz();
        double[][] M1=m.getMatriz();
        double[][] M2=this.getMatriz();
        
        int linhasMatriz = M1.length;
        int colunasMatriz = M1[0].length;
        int linhasMatriz2 = M2.length;
        int colunasMatriz2 = M2[0].length;
        if(linhasMatriz != linhasMatriz2 || colunasMatriz != colunasMatriz2){
            throw new SomaMatrizesIncompativeisException(m,this);
        }

        Matriz resultadoMatriz = new Matriz(linhasMatriz, colunasMatriz);
        double[][] r = resultadoMatriz.getMatriz();
        for (int i=0;i<linhasMatriz;i++)
        {
            for(int j=0;j<colunasMatriz;j++)
            {
               r[i][j]=M1[i][j]+M2[i][j];
               //r[i][j]=(double)Math.round(r[i][j]*1000)/1000;
            }
        }
        return resultadoMatriz;
    }

    
    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     */
    public Matriz prod(Matriz m) throws ProdMatrizesIncompativeisException, MatrizInvalidaException {
        //throw new UnsupportedOperationException("Produto de matrizes não implementado.");
        //ProdMatrizesIncompativeisException P = new ProdMatrizesIncompativeisException();
        double[][] m1 = m.getMatriz();
        double[][] n1 = this.getMatriz();
        int linhasMatriz = m1.length;
        int colunasMatriz = m1[0].length;
        int linhasMatrizn = n1.length;
        int colunasMatrizn = n1[0].length;
        if(colunasMatriz!=linhasMatrizn) {
            throw new ProdMatrizesIncompativeisException(m,this);
        /*    m1 = this.getMatriz();
            n1 = m.getMatriz();
            linhasMatriz = n1[0].length;
            colunasMatriz = n1.length;
            linhasMatrizn = m1[0].length;
            colunasMatrizn = m1.length; */
        }
        Matriz resultadoMatriz = new Matriz(linhasMatriz, colunasMatrizn);
        double[][] r = resultadoMatriz.getMatriz();
        for (int i=0;i<linhasMatriz;i++)
        {
            for(int j=0;j<colunasMatrizn;j++)
            {
                r[i][j]=0;
                for(int k=0;k<linhasMatrizn;k++)
                {
                    r[i][j]=r[i][j]+m1[i][k]*n1[k][j];
                    //r[i][j]=(double)Math.round(r[i][j]*1000)/1000;
//                    System.out.println("i = "+i+"j = "+j+"k = "+k+"resultado = "+r[i][j]+"\n");
                }
            }    
        }
//        System.out.println("Matriz = "+resultadoMatriz+"\nm2 [0][1] = "+m2[0][1]+"\nTamanho Linhas = "+linhasMatriz+"\nTamanho Colunas = "+colunasMatriz);
        return resultadoMatriz;
    }
   

    /**
     * Retorna uma representação textual da matriz.
     * Este método não deve ser usado com matrizes muito grandes
     * pois não gerencia adequadamente o tamanho do string e
     * poderia provocar um uso excessivo de recursos.
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha: mat) {
            s.append("[ ");
            for (double x: linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }
    
}
